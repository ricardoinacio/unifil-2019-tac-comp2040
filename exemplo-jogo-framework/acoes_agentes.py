from enum import Enum
class AcoesJogador(Enum):
    PERMUTAR = "permutar"

from dataclasses import dataclass
@dataclass
class AcaoJogador():
    tipo: AcoesJogador
    parametros: tuple = tuple()
    
    def __str__(self):
        return f'{self.tipo.value}{self.parametros}'
    
    @classmethod
    def permutar(cls, i, j):
        """ Cria uma instância da classe AcaoJogador representando a acao
        permutar elementos das posicoes i e j.
        """
        return cls(AcoesJogador.PERMUTAR, (i,j))