class ExpressaoPredicado():
    """ TODO: implementar ExpressaoPredicado como ADT, verificar forma pitônica
    de faze-lo.
    
    Em Haskell seria:
    `data ExpressaoPredicado = Constante str | Variavel str | [ExpressaoPredicado]`
    """
    def isConstante(self):
        return
        
    def isVariavel(self):
        return
    
    def isLista(self):
        return


def traduzir(subs, expr):
    expr_traduzida = ExpressaoPredicado()
    for termo in enumerate(expr):
        expr_traduzida.append(traduzir(subs, termo) if termo.isLista()
            else subs.get(termo, default=termo))
    
    return expr_traduzida


def compor_subs(s, sl):
    composicao = {}
    for expr_key, expr_sub in s.items():
        composicao[expr_key] = traduzir(sl, expr_sub)
    
    composicao.update(sl)
    return composicao
    

def unificar(expr_esq, expr_dir):
    # Casos base, pattern matching nos tipos
    if all(expr.isConstante() or len(expr) == 0 for expr in (expr_esq, expr_dir)):
        return dict() if expr_esq == expr_dir else None
    
    if expr_esq.isVariavel():
        return None if expr_esq in expr_dir else {expr_esq: expr_dir}
    
    if expr_dir.isVariavel():
        return None if expr_dir in expr_esq else {expr_dir: expr_esq}
    
    if any(len(expr) == 0 for expr in (expr_esq, expr_dir)):
        return None
    
    # Divisao e conquista, ambas exprs são listas
    subs_heads = unificar(expr_esq[0], expr_dir[0])
    if subs_heads == None:
        return None

    tail_esq = traduzir(subs_heads, expr_esq[1:])
    tail_dir = traduzir(subs_heads, expr_dir[1:])
    subs_tails = unificar(tail_esq, tail_dir)
    return compor_subs(subs_heads, subs_tails) if subs_tails != None else None




