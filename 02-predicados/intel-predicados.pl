/% Use swi-prolog ou swish (swi-prolog web) para rodar o código abaixo.

bloco(a).
bloco(b).
bloco(c).
sobre(b,a).
sobre_mesa(c).
sobre_mesa(b).

livre(BlocoA) :-
    bloco(BlocoA), bloco(BlocoB), sobre(BlocoB,BlocoA).

livre(BlocoA) :-
    sobre_mesa(BlocoA), not(sobre(BlocoA,_)).
