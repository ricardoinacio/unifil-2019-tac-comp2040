#!/usr/bin/env python3

def subida_encosta(estado_atual, fa):
    """ (estado, n -> int ) -> estado
    estado_atual é um estado inicial do problema e fa é uma função de
    avaliação. É retornado o estado de menor fa em uma busca sem
    retrocesso. Não há garantia que a solução tenha fa(n) == 0.
    """
    while True:
        estado_adjacentes = (estado_atual.transicao(acao) 
            for acao in estado_atual.acoes_possiveis())
        
        # argmin
        estado_minimo = min(estado_adjacentes, key=fa)
        if fa(estado_minimo) < fa(estado_atual):
            estado_atual = estado_minimo
        else:
            # Não há garantias de f_heuristica == 0
            return estado_atual
